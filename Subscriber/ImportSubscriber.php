<?php

namespace HrRatingImport\Subscriber;

use Enlight\Event\SubscriberInterface;
use Psr\Container\ContainerInterface;
use HrRatingImport\Services\ImportService;
use Shopware_Components_Cron_CronJob;

class ImportSubscriber implements SubscriberInterface
{
    private ContainerInterface $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'Shopware_CronJob_HrRatingImport' => 'onCronImport'
        ];
    }

    /**
     * @param Shopware_Components_Cron_CronJob $job
     *
     * @return string
     */
    public function onCronImport(Shopware_Components_Cron_CronJob $job): string
    {
        return $this->container->get(ImportService::class)->importRatings();
    }
}
