<?php

namespace HrRatingImport\Controller\Frontend;

use Enlight_Controller_Action;
use Enlight_Exception;
use Exception;
use ZipArchive;

class downloadRatingImporter extends Enlight_Controller_Action
{
    private array $configs;

    private string $txtFile;

    private string $zipFile;

    public function __construct(string $downloadDir, array $configs)
    {
        $this->configs = $configs;
        $this->txtFile = $downloadDir . "uploadRating.txt";
        $this->zipFile = $downloadDir . "uploadRatings.zip";

        parent::__construct();
    }

    /**
     * @throws Exception|Enlight_Exception
     */
    public function preDispatch(): void
    {
        $this->Front()->Plugins()->ViewRenderer()->setNoRender();
    }

    public function indexAction(): void
    {
        if (!file_exists($this->zipFile)) {
            echo "Bitte legen die uploadRatings.zip in das download Verzeichnis!";

            return;
        }

        $this->createUploadFile();
        $this->addNewFileToZip();
        $this->fileDownload();
    }

    private function createUploadFile(): void
    {
        $content = "# Connect to SFTP server using a password\r\n";
        $content .= "open sftp://{$this->configs['ftp_user']}:{$this->configs['ftp_password']}@{$this->configs['ftp_server']} -hostkey=*\r\n";
        $content .= "# Upload file\r\n";
        $content .= "put %1% /\r\n";
        $content .= "# Exit WinSCP\r\n";
        $content .= "exit\r\n";

        if (file_exists($this->txtFile)) {
            unlink($this->txtFile);
        }

        $fh = fopen($this->txtFile, 'wb');
        fwrite($fh, $content);
        fclose($fh);
    }

    private function addNewFileToZip(): void
    {
        $zip = new ZipArchive();
        if ($zip->open($this->zipFile, ZipArchive::CREATE)) {
            $zip->addFile($this->txtFile, "uploadRating.txt");
        }
        $zip->close();
    }

    private function fileDownload(): void
    {

        header("Content-type: application/zip");
        header("Content-Disposition: attachment; filename=uploadRatings.zip");
        header("Content-length: " . filesize($this->zipFile));
        header("Pragma: no-cache");
        header("Expires: 0");
        ob_end_flush();
        @readfile($this->zipFile);
    }
}
