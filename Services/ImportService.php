<?php

namespace HrRatingImport\Services;

use DateTime;
use Doctrine\DBAL\Connection;
use Doctrine\DBAL\Exception;
use RuntimeException;

class ImportService
{
    public const IMPORT_REQUIREMENTS = [
        'articleID',
        'name',
        'headline',
        'comment',
        'points',
        'datum',
    ];
    public const CSV_SEPARATOR       = ';';
    public const DEBUG_CONSOLE       = false;
    public const CRLF                = "\r\n";

    /**
     * @var Connection
     */
    private Connection $connection;

    private array $config;

    private string $file;

    private string $importFilePath;

    private string $importFolder;

    public function __construct(Connection $connection, $config, $rootDir)
    {
        $this->connection     = $connection;
        $this->config         = $config;
        $this->importFolder   = 'files/import_cron/';
        $this->importFilePath = $rootDir . $this->importFolder;
    }

    /**
     * @return string
     */
    private function getFile(): string
    {
        $file = $this->importFilePath . $this->config['hrImportFileName'];

        return file_exists($file) ? $file : "";
    }

    /**
     * @throws Exception|\Doctrine\DBAL\Driver\Exception
     */
    public function importRatings(): string
    {
        $this->file = $this->getFile();

        if (empty($this->file)) {
            return 'No ' . $this->config['hrImportFileName'] . ' found in ' . $this->importFolder;
        }

        if (!$this->validateImportFile()) {
            return 'File not valid. Please check headline.';
        }

        $fp      = fopen($this->file, "rb");
        $head    = fgetcsv($fp, 1000, self::CSV_SEPARATOR);
        $headArr = array_change_key_case(array_flip($head), CASE_LOWER);

        $imported = 0;
        $total    = 0;

        while (($rowArr = fgetcsv($fp, 1000, self::CSV_SEPARATOR))) {
            $rating = [
                'active'  => 1,
                'shop_id' => 1,
            ];

            $total++;

            foreach (self::IMPORT_REQUIREMENTS as $req) {
                switch ($req) {
                    case 'datum':
                        $rating[$req] = $this->transformDate($rowArr[$headArr[strtolower($req)]]);
                        break;

                    case 'articleID':
                        $rating[$req] = $this->getArticleId($rowArr[$headArr[strtolower($req)]]);
                        break;

                    default:
                        $rating[$req] = utf8_encode($rowArr[$headArr[strtolower($req)]]);
                        break;
                }
            }

            $exists = $this->preventDuplicates($rating);
            if (!self::DEBUG_CONSOLE && !$exists) {
                $imported++;
                $this->connection->insert('s_articles_vote', $rating);
            } else {
                if (!$exists) {
                    $imported++;
                }
                echo print_r($rating, 1);
            }
        }

        $this->moveFile();

        return "Imported $imported/$total from " . $this->config['hrImportFileName'] . ' and moved file to done!';
    }

    /**
     * @param $rating
     *
     * @return bool
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private function preventDuplicates($rating): bool
    {
        $qb = $this->connection->createQueryBuilder();

        $r = $qb->select('*')
                ->from('s_articles_vote')
                ->where('name = :name')
                ->andWhere('articleID = :articleID')
                ->setParameter('name', $rating['name'])
                ->setParameter('articleID', $rating['articleID'])
                ->execute()->fetchAllAssociative();

        return !empty($r);
    }

    /**
     * @param $dateString
     *
     * @return string
     */
    private function transformDate($dateString): string
    {
        $date = new DateTime();
        $date->setTimestamp(strtotime($dateString));

        return $date->format('Y-m-d H:i:s');
    }

    /**
     * @return bool
     */
    private function validateImportFile(): bool
    {
        $fp   = fopen($this->file, "rb");
        $head = fgetcsv($fp, 1000, self::CSV_SEPARATOR);
        fclose($fp);

        return count(array_intersect($head, self::IMPORT_REQUIREMENTS)) === count(self::IMPORT_REQUIREMENTS);
    }

    private function moveFile(): void
    {
        $folder = $this->ensureFolderExists("done");
        $target = $folder . time() . '_' . $this->config['hrImportFileName'];

        if (self::DEBUG_CONSOLE) {
            $this->debug('Old Dir and Name: ' . $this->file);
            $this->debug('New Dir and Name: ' . $target);
        } else {
            rename($this->file, $target);
        }
    }

    /**
     * @param string $folder
     *
     * @return string
     */
    private function ensureFolderExists(string $folder): string
    {
        $fp = $this->importFilePath . $folder . DIRECTORY_SEPARATOR;
        if (!is_dir($fp) && !mkdir($fp, 0777, true) && !is_dir($fp)) {
            throw new RuntimeException(sprintf('Directory "%s" was not created', $fp));
        }

        return $fp;
    }

    /**
     * @param $ordernumber
     *
     * @return bool|string
     * @throws Exception
     * @throws \Doctrine\DBAL\Driver\Exception
     */
    private function getArticleId($ordernumber): string
    {
        $qb = $this->connection->createQueryBuilder();

        return $qb->select('articleID')
                  ->from('s_articles_details')
                  ->where('ordernumber = :ordernumber')
                  ->setParameter('ordernumber', $ordernumber)
                  ->execute()->fetchOne();
    }

    private function debug($msg, $d = false, $l = 0): void
    {
        if (is_array($msg)) {
            echo "<pre>";
            echo print_r($msg, 1);
            echo "</pre>";
        } else {
            echo "<pre>$msg</pre>";
        }

        if ($d) {
            echo die("Abort @ Line " . $l);
        }
    }
}
